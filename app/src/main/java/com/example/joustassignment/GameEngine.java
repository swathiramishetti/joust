package com.example.joustassignment;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;

public class GameEngine extends SurfaceView implements Runnable, GestureDetector.OnGestureListener {

    // -----------------------------------
    // ## ANDROID DEBUG VARIABLES
    // -----------------------------------

    // Android debug variables
    final static String TAG="JOUST-GAME";

    // -----------------------------------
    // ## SCREEN & DRAWING SETUP VARIABLES
    // -----------------------------------

    // screen size
    int screenHeight;
    int screenWidth;

    // game state
    boolean gameIsRunning;
    boolean gameOver = false;
    boolean win = false;


    // threading
    Thread gameThread;


    // drawing variables
    SurfaceHolder holder;
    Canvas canvas;
    Paint paintbrush;


    private GestureDetector gestureDetector;

    // -----------------------------------
    // ## GAME SPECIFIC VARIABLES
    // -----------------------------------
//Created an array to store  enemies
    ArrayList<Enemy> enemies = new ArrayList<Enemy>();
    ArrayList<Enemy> eggs = new ArrayList<Enemy>();
    ArrayList<Integer> eggTimer = new ArrayList<Integer>();



    // ----------------------------
    // ## SPRITES
    // ----------------------------

    Bitmap levels;
    Enemy enemy;
    Player player;
    Player playerHeight;
    Enemy egg;


    int newYPosition = 0;

    Boolean playerUp = false;
    Boolean playerDown = false;

    String PlayerTowards = "static";
    int playerLevel = 4;
    int eggX = 0;
    int eggY = 0;
    int enemyCount = 0;


    // ----------------------------
    // ## GAME STATS - number of lives, score, etc
    // ----------------------------
    int lives = 1;
    int score = 0;



    public GameEngine(Context context, int w, int h) {
        super(context);


        this.holder = this.getHolder();
        this.paintbrush = new Paint();

        this.screenWidth = w;
        this.screenHeight = h;

        gestureDetector = new GestureDetector(getContext(),this);

        this.printScreenInfo();

        // @TODO: Add your sprites to this section
        // This is optional. Use it to:
        //  - setup or configure your sprites
        //  - set the initial position of your sprites


        this.levels = BitmapFactory.decodeResource(context.getResources(), R.drawable.levels);
        levels = Bitmap.createScaledBitmap(levels, screenWidth,80,false);
        enemy = new Enemy(getContext(), 100, 200, R.drawable.enemy);
        playerHeight = new Player(getContext(),0,0,R.drawable.happy);
        this.player = new Player(getContext(), 50, (screenHeight - 400) - this.playerHeight.getImage().getHeight(), R.drawable.happy);


        // @TODO: Any other game setup stuff goes here


    }

    // ------------------------------
    // HELPER FUNCTIONS
    // ------------------------------
// make enemy
    public void makeEnemy(int xs, int ys) {
        enemy = new Enemy(getContext(), xs, ys, R.drawable.enemy);
        enemies.add(enemy);
    }
    // MAKE EGG
    public void makeEgg(int xs, int ys) {
        egg = new Enemy(getContext(), xs, ys, R.drawable.orange);
        eggs.add(egg);
    }

// generate random levels
    public int generateRandomLevel() {
        Random r = new Random();
        int level = r.nextInt(5);
        if (level == 1) {
            return  screenHeight - 1600;
        }
        else if (level == 2) {
            return screenHeight - 1200;
        }
        else if (level == 3) {
            return screenHeight - 800;
        }
        else if (level == 4) {
            return screenHeight - 400;
        }
        return 0;
    }
    // To get new y positions while jumping
    public int getnewYPosition(int level){
        int newYPosition = screenHeight - 400 - this.playerHeight.getImage().getHeight();;
        if (level ==  1) {
            newYPosition = screenHeight - 1600 - this.playerHeight.getImage().getHeight();
        } else if (level == 2) {
            newYPosition = screenHeight - 1200 - this.playerHeight.getImage().getHeight();
        } else if (level == 3) {
            newYPosition = screenHeight - 800 - this.playerHeight.getImage().getHeight();
        } else if (level == 4) {
            newYPosition = screenHeight - 400 - this.playerHeight.getImage().getHeight();
        }
        return newYPosition;
    }

    // This funciton prints the screen height & width to the screen.
    private void printScreenInfo() {

        Log.d(TAG, "Screen (w, h) = " + this.screenWidth + "," + this.screenHeight);
    }


    // ------------------------------
    // GAME STATE FUNCTIONS (run, stop, start)
    // ------------------------------
    @Override
    public void run() {
        while (gameIsRunning == true) {
            this.updatePositions();
            this.redrawSprites();
            this.setFPS();
        }
    }


    public void pauseGame() {
        gameIsRunning = false;
        try {
            gameThread.join();
        } catch (InterruptedException e) {
            // Error
        }
    }

    public void startGame() {
        gameIsRunning = true;
        gameThread = new Thread(this);
        gameThread.start();
    }


    // ------------------------------
    // GAME ENGINE FUNCTIONS
    // - update, draw, setFPS
    // ------------------------------



    // 1. Tell Android the (x,y) positions of your sprites
    public void updatePositions() {
        // @TODO: Update the position of the sprites




                // @TODO: Collision detection code

                //player jumping
                if (PlayerTowards != "static") {

                    if (PlayerTowards == "Right") {
                        if (this.player.getxPosition() >= this.screenWidth) {
                            this.player.setxPosition((0 - this.player.getImage().getWidth()));
                        }
                        this.player.setxPosition(this.player.getxPosition() + 30);
                        this.player.updateHitboxBottom();
                        this.player.updateHitBoxTop();
                    } else if (PlayerTowards == "Left") {
                        if ((this.player.getxPosition() + this.player.getImage().getWidth()) <= 0) {
                            this.player.setxPosition(this.screenWidth);
                        }
                        this.player.setxPosition(this.player.getxPosition() - 30);
                        this.player.updateHitboxBottom();
                        this.player.updateHitBoxTop();


                    } else if (PlayerTowards == "Down") {
                        if (playerLevel != 4)
                            playerLevel++;
                        else
                            playerLevel = 1;
                        int level = this.playerLevel;
                        PlayerTowards = "static";
                        playerDown = true;

                        newYPosition = getnewYPosition(level);

                    } else if (PlayerTowards == "Up") {
                        if (playerLevel != 1)
                            playerLevel--;
                        else
                            playerLevel = 4;

                        int level = this.playerLevel;
                        PlayerTowards = "static";
                        playerUp = true;
                        newYPosition = getnewYPosition(level);

                    }

                }


                if (this.player.getyPosition() != newYPosition && playerUp == true) {

                    this.player.setyPosition(this.player.getyPosition() - 230);


                    if (this.player.getyPosition() <= newYPosition) {
                        this.player.setyPosition(newYPosition);
                        playerUp = false;
                    }
                    this.player.updateHitboxBottom();
                    this.player.updateHitBoxTop();

                }

                if (this.player.getyPosition() != newYPosition && playerDown == true) {

                    if (this.player.getyPosition() <= newYPosition) {
                        this.player.setyPosition(this.player.getyPosition() + 230);
                    } else if (this.player.getyPosition() > newYPosition) {
                        this.player.setyPosition(0);
                        this.player.setyPosition(this.player.getyPosition() + 230);
                    }
                    if ((this.player.getyPosition() >= 0) && (this.player.getyPosition() >= newYPosition)) {
                        this.player.setyPosition(newYPosition);
                        playerDown = false;
                    }
                    this.player.updateHitboxBottom();
                    this.player.updateHitBoxTop();
                }

        //updating enemy positions
        if (enemies.size() > 0 || eggs.size() > 0 ) {
            for (int i = 0; i < enemies.size(); i++) {
                Enemy enemy = enemies.get(i);
                enemy.setxPosition(enemy.getxPosition() + 15);

                enemy.updateHitbox();
                if(enemy.getxPosition() > screenWidth){
                    enemy.setxPosition(- (enemy.getxPosition()));
                }

                // plyer toutches enemy from bottom
                if(enemy.getHitbox().intersect(this.player.getHitboxTop()))
                {
                    if((player.getyPosition() != screenHeight-400 - this.playerHeight.getImage().getHeight()) || (player.getxPosition() != 50)) {
                        PlayerTowards = "Static";
                        playerLevel = 4;
                        playerUp = false;
                        playerDown = false;
                        player.setxPosition(50);
                        player.setyPosition(screenHeight-400 - this.playerHeight.getImage().getHeight());
                        player.updateHitBoxTop();
                        player.updateHitboxBottom();
                        lives = lives -1;

                    }

                }

                // decide if you should be game over:
                if (this.lives == 0 || this.enemyCount == 0) {
                    this.gameOver = true;
                    enemy.setxPosition(enemy.getxPosition());

                }

                if (this.score == 8) {
                    this.win = true;
                    enemy.setxPosition(enemy.getxPosition());
                                  }

                // plyer toutches enemy from top
                if(enemy.getHitbox().intersect(this.player.getHitboxBottom()))
                {
                    eggX = enemy.getxPosition();
                    eggY = enemy.getyPosition();


                    makeEgg((int) ((Math.random() * (((this.screenWidth - this.enemy.image.getWidth()))))),
                            this.generateRandomLevel() - this.enemy.image.getHeight());

                    eggTimer.add((int) System.currentTimeMillis());
                    enemies.remove(enemy);
                    score = score + 1;
                }


            }
            // player collision with egg
            for(int i = 0; i<eggs.size(); i++) {

                //detecting player collision with egg
                if (player.getHitboxTop().intersect(eggs.get(i).getHitbox())
                        || player.getHitboxBottom().intersect(eggs.get(i).getHitbox())) {
                    eggs.remove(i);
                    eggTimer.remove(i);

                }
            }
            // remove egg after 10 ms.
            for(int i =0; i<eggs.size();i++)
            {

                if((int) System.currentTimeMillis() - eggTimer.get(i)  > 10000)
                {
                    makeEnemy(eggs.get(i).getxPosition(),eggs.get(i).getyPosition());
                    eggs.remove(i);
                    eggTimer.remove(i);
                }
            }

        }

                // @TODO: Collision detection code

            }

            long currentTime = 0;
            long previousTime = 0;

            // 2. Tell Android to DRAW the sprites at their positions
            public void redrawSprites(){
                if (this.holder.getSurface().isValid()) {
                    this.canvas = this.holder.lockCanvas();

                    //----------------
                    // Put all your drawing code in this section

                    // configure the drawing tools
                    this.canvas.drawColor(Color.argb(255, 0, 0, 255));
                    paintbrush.setColor(Color.WHITE);


                    //@TODO: Draw the sprites (rectangle, circle, etc)


                    // Draw the levels on screen
                    canvas.drawBitmap(levels, 0, screenHeight - 1600, paintbrush);
                    canvas.drawBitmap(levels, 0, screenHeight - 1200, paintbrush);
                    canvas.drawBitmap(levels, 0, screenHeight - 800, paintbrush);
                    canvas.drawBitmap(levels, 0, screenHeight - 400, paintbrush);

                    // draw player
                    this.canvas.drawBitmap(this.player.getImage(), this.player.getxPosition(), this.player.getyPosition(), paintbrush);

                    // draw enmies
                    currentTime = System.currentTimeMillis();

                    if ((currentTime - previousTime) > 3000) {


                        if (enemies.size() < 10) {
                            makeEnemy((int) ((Math.random() * (((this.screenWidth - this.enemy.image.getWidth()) - 0) + 1)) + 0),
                                    this.generateRandomLevel() - this.enemy.image.getHeight());
                            enemyCount++;                        }
                        previousTime = currentTime;
                    }
                    if (enemies.size() > 0) {
                        for (int i = 0; i < enemies.size(); i++) {
                            Enemy enemy = enemies.get(i);
                            canvas.drawBitmap(enemy.getImage(), enemy.getxPosition(), enemy.getyPosition(), paintbrush);
                            paintbrush.setColor(Color.WHITE);
                            paintbrush.setStyle(Paint.Style.STROKE);
                            paintbrush.setStrokeWidth(5);
                            enemy.getHitbox();
                        }
                    }

                    for(int i =0; i< eggs.size(); i++) {
                        Enemy egg = eggs.get(i);
                        canvas.drawBitmap(egg.getImage(), egg.getxPosition(), egg.getyPosition(), paintbrush);
                        paintbrush.setColor(Color.WHITE);
                        paintbrush.setStyle(Paint.Style.STROKE);
                        paintbrush.setStrokeWidth(5);
                        egg.getHitbox();
                    }




                    //@TODO: Draw game statistics (lives, score, etc)
                    if (gameOver == true) {
                                   canvas.drawText("GAME OVER!", screenWidth/2, screenHeight/2, paintbrush);

                    }
                    if(win == true){
                        canvas.drawText("YOU WIN", screenWidth/2, screenHeight/2, paintbrush);


                    }
                    paintbrush.setTextSize(60);
                    canvas.drawText(("Score: " + score), screenWidth-300, 100, paintbrush);
                    this.canvas.drawText(("Lives: " + lives), 20, 100, paintbrush);

                    //----------------
                    this.holder.unlockCanvasAndPost(canvas);
                }
            }
    public void setFPS() {
        try {
            gameThread.sleep(50);
        } catch (Exception e) {

        }
    }


            // ------------------------------
            // USER INPUT FUNCTIONS
            // ------------------------------
            @Override
            public boolean onTouchEvent(MotionEvent event) {
                // TODO Auto-generated method stub

                return gestureDetector.onTouchEvent(event);
            }




    @Override
    public boolean onDown(MotionEvent e) {
        return true;
    }

    @Override
    public void onShowPress(MotionEvent e) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return true;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return true;
    }

    @Override
    public void onLongPress(MotionEvent e) {

    }

    @Override
    public boolean onFling(MotionEvent motionEvent1, MotionEvent motionEvent2, float velocityX, float velocityY) {
        if (motionEvent1.getY() - motionEvent2.getY() > 100) {

            onSwipeUp();

        }

        if (motionEvent2.getY() - motionEvent1.getY() > 100) {

            onSwipeDown();

        }

        if (motionEvent1.getX() - motionEvent2.getX() > 100) {

            onSwipeLeft();
        }

        if (motionEvent2.getX() - motionEvent1.getX() > 100) {


            onSwipeRight();

            return true;
        }
        else {

            return true;
        }
    }

    private void onSwipeLeft() {
        Toast.makeText(getContext(), " Swipe Left ", Toast.LENGTH_LONG).show();
        PlayerTowards = "Left";
    }
    private void onSwipeRight() {
        Toast.makeText(getContext(), " Swipe Right ", Toast.LENGTH_LONG).show();
        PlayerTowards = "Right";
    }
    private void onSwipeUp() {
        Toast.makeText(getContext(), " Swipe Up ", Toast.LENGTH_LONG).show();
        PlayerTowards = "Up";
    }

    private void onSwipeDown() {
        Toast.makeText(getContext(), " Swipe Down ", Toast.LENGTH_LONG).show();
        PlayerTowards = "Down";
    }





}